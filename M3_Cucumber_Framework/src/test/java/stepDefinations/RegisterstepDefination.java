package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class RegisterstepDefination {
	File dir;
	String endpoint;
	String requestbody;
	Response response;
	int status_code;
	String req_email;
	String req_pasw;
	String res_id;
	String res_token;

	@Given("enter email and password in requestbody")
	public void enter_email_and_password_in_requestbody() throws IOException {
		dir = Utility.createLogDirectory("Api_logs");
		endpoint = Data_repositery.hostname() + Data_repositery.resource_reg_succ();
		requestbody = RequestBody.req_post_reg_succ("post_TC1");
		response = Api_trigger.post_reg_succ_trigger(Data_repositery.headername(), Data_repositery.headervalue(),
				requestbody, endpoint);
		Utility.evidenceFileCreator(dir, Utility.testLogName("post_reg_succ"), endpoint, requestbody,
				response.getHeader("Date"), response.getBody().asPrettyString());
	}

	@When("trigger the API with given payload")
	public void trigger_the_api_with_given_payload() {
		status_code = response.getStatusCode();
		
		res_id = response.getBody().jsonPath().getString("id");
		res_token = response.getBody().jsonPath().getString("token");
	}

	@Then("validate ststus code")
	public void validate_ststus_code() {
		Assert.assertEquals(status_code, 200);
	}

	@Then("validate response body parameters")
	public void validate_response_body_parameters() {
		JsonPath req_jsn = new JsonPath(requestbody);
		req_email = req_jsn.getString("email");
		req_pasw = req_jsn.getString("password");
		
		Assert.assertNotNull(res_id);
		Assert.assertNotNull(res_token);

	}
}
