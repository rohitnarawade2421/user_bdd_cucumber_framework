package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class PoststepDefination {
	String requestbody;
	String endpoint;
	Response response;
	File dir_name;
	int status_code1;
	String req_name;
	String req_job;
	String res_name;
	String res_job;
	String res_id;
	String res_createdat;
	String exp_time;

	@Before
	public void beforeScenario() {
	System.out.println("This will run before scenario");	
	}
	@After
	public void afterScenario() {
		System.out.println("This will run after scenario");
	}

	@Given("Name and Job in Request Body")
	public void name_and_job_in_request_body() throws IOException {
		dir_name = Utility.createLogDirectory("Api_logs");
		requestbody = RequestBody.req_post_create("post_TC1");
		endpoint = Data_repositery.hostname() + Data_repositery.resource_post_create();
		response = Api_trigger.post_trigger(Data_repositery.headername(), Data_repositery.headervalue(), requestbody,
				endpoint);
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("post_create"), endpoint, requestbody,
				response.getHeader("date"), response.getBody().asPrettyString());
	}

	@When("Send the Request with Payload to the Endpoint")
	public void send_the_request_with_payload_to_the_endpoint() {
		status_code1 = response.getStatusCode();
		res_name = response.getBody().jsonPath().getString(RequestBody.key_innameORemail);
		res_job = response.getBody().jsonPath().getString(RequestBody.key_injobORpasw);
		res_id = response.getBody().jsonPath().getString("id");
		res_createdat = response.getBody().jsonPath().getString("createdAt");
		res_createdat = res_createdat.substring(0, 11);

	}

	@Then("Validate Status Code")
	public void validate_status_code() {
		Assert.assertEquals(status_code1, 201);
	}

	@Then("Validate Resopnse Body Parameters")
	public void validate_resopnse_body_parameters() {
		JsonPath req_jsn = new JsonPath(requestbody);
		req_name = req_jsn.getString(RequestBody.key_innameORemail);
		req_job = req_jsn.getString(RequestBody.key_injobORpasw);

		LocalDateTime curranttime = LocalDateTime.now();
		exp_time = curranttime.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdat, exp_time);

		
		
	}

}
