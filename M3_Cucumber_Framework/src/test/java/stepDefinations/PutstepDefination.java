package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class PutstepDefination {
	String endpoint;
	String requestbody;
	File dir_name;
	Response response;
	int StatusCcode;
	String res_name;
	String res_job;
	String res_time;

	@Given("enter valid name and job in put_api requestbody")
	public void enter_valid_name_and_job_in_put_api_requestbody() throws IOException {
		
		endpoint = Data_repositery.hostname() + Data_repositery.resource_put_update();
		requestbody = RequestBody.req_put_update("put_TC1");
		dir_name = Utility.createLogDirectory("Api_logs");
		
		response = Api_trigger.put_update(Data_repositery.headername(), Data_repositery.headervalue(), requestbody,
				endpoint);
		
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("put_update"), endpoint, requestbody,
				response.getHeader("Date"), response.getBody().asPrettyString());

	}

	@When("trigger the api to hit the put_api endpoint")
	public void trigger_the_api_to_hit_the_put_api_endpoint() {
		
		StatusCcode = response.getStatusCode();
		
		res_name = response.getBody().jsonPath().getString(RequestBody.key_innameORemail);
		System.out.println("name:" + res_name);
		res_job = response.getBody().jsonPath().getString(RequestBody.key_injobORpasw);
		System.out.println("job:" + res_job);
		res_time = response.getBody().jsonPath().getString("updatedAt");
		System.out.println("updated at:" + res_time);
		res_time = res_time.substring(0, 11);
		System.out.println(res_time);
	}

	@Then("validate status code of put_api")
	public void validate_status_code_of_put_api() {
		Assert.assertEquals(StatusCcode, 200);
	}

	@Then("validate response body parameters of put_api")
	public void validate_response_body_parameters_of_put_api() {
		
		JsonPath req_jsn = new JsonPath(requestbody);
		String req_name = req_jsn.getString(RequestBody.key_innameORemail);
		String req_job = req_jsn.getString(RequestBody.key_injobORpasw);
		
		LocalDateTime curranttime = LocalDateTime.now();
		String exp_time = curranttime.toString().substring(0, 11);
		
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_time, exp_time);

	}
}
