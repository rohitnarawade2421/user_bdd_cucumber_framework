package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class DataDrivenstepDefination {
	String requestbody;
	String endpoint;
	Response response;
	File dir_name;
	int status_code1;
	String req_name;
	String req_job;
	String res_name;
	String res_job;
	String res_id;
	String res_createdat;
	String exp_time;

	
	
	@Given("Enter {string} and {string} in post request body")
	public void enter_and_in_post_request_body(String req_name, String req_job) throws IOException {
		requestbody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
		dir_name = Utility.createLogDirectory("Api_logs");
		endpoint = Data_repositery.hostname() + Data_repositery.resource_post_create();
		response = Api_trigger.post_trigger(RequestBody.headername(), RequestBody.headervalue(),
				requestbody, endpoint);
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("post_create"), endpoint, requestbody,
				response.getHeader("date"), response.getBody().asString());
	}

	@When("Send the post request with data")
	public void send_the_post_request_with_data() {
		status_code1 = response.getStatusCode();
		res_name = response.getBody().jsonPath().getString("name");
		res_job = response.getBody().jsonPath().getString("job");
		res_id = response.getBody().jsonPath().getString("id");
		res_createdat = response.getBody().jsonPath().getString("createdAt");
		res_createdat = res_createdat.substring(0, 11);

	}
	@Then("Validate data_driven_post status code")
	public void validate_data_driven_post_status_code() {
		Assert.assertEquals(status_code1, 201);
	}
	@Then("Validate data_driven_post response body parameters")
	public void validate_data_driven_post_response_body_parameters() {
		JsonPath req_jsn = new JsonPath(requestbody);
		req_name = req_jsn.getString("name");
		req_job = req_jsn.getString("job");

		LocalDateTime curranttime = LocalDateTime.now();
		exp_time = curranttime.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdat, exp_time);

	}

}
