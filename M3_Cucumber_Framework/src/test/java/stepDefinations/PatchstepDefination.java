package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class PatchstepDefination {
	String endpoint;
	String requestbody;
	File dir_name;
	Response response;
	int StatusCcode;
	String res_name;
	String res_job;
	String res_time;
	String req_name;
	String req_job;

	@Given("enter valid name and job in requestbody of patch_api")
	public void enter_valid_name_and_job_in_requestbody_of_patch_api() throws IOException {
		endpoint = Data_repositery.hostname() + Data_repositery.resource_patch_update();
		requestbody = RequestBody.req_patch_update();
		dir_name = Utility.createLogDirectory("Api_logs");
		response = Api_trigger.patch_update_trigger(Data_repositery.headername(), Data_repositery.headervalue(),
				requestbody, endpoint);
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("patch_update"), endpoint, requestbody,
				response.getHeader("Date"), response.asPrettyString());
	}

	@When("trigger the api to hit the endpoint of patch_api")
	public void trigger_the_api_to_hit_the_endpoint_of_patch_api() {
		StatusCcode = response.getStatusCode();

		res_name = response.getBody().jsonPath().getString("name");
		System.out.println("name:" + res_name);
		res_job = response.getBody().jsonPath().getString("job");
		System.out.println("job:" + res_job);
		res_time = response.getBody().jsonPath().getString("updatedAt");
		System.out.println("updated at:" + res_time);
		res_time = res_time.substring(0, 11);
		System.out.println(res_time);

	}

	@Then("validate status code for patch_api")
	public void validate_status_code_for_patch_api() {
		Assert.assertEquals(StatusCcode, 200);
	}

	@Then("validate response body parameters of patch_api")
	public void validate_response_body_parameters_of_patch_api() {

		JsonPath req_jsn = new JsonPath(requestbody);
		req_name = req_jsn.getString("name");
		req_job = req_jsn.getString("job");

		LocalDateTime curranttime = LocalDateTime.now();
		String exp_time = curranttime.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_time, exp_time);

	}

}
