package stepDefinations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Repositery.Data_repositery;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetstepDefination {
	String endpoint;
	File log_dir;
	Response response;
	String res_body;
	int statusCcode;
	JsonPath res_jsn;
	int count;
	int i;
	
	int idArr[];
	String emailArr[];
	String fnamesArr[];
	String lnamesArr[];

	
	int ids[] = { 7, 8, 9, 10, 11, 12 };
	String Emails[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
			"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
	String firstNames[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
	String lastNames[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };

	
	@Given("configure get list user endpoint")
	public void configure_get_list_user_endpoint() throws IOException {
		endpoint = Data_repositery.hostname() + Data_repositery.resource_get_user_list();
		log_dir = Utility.createLogDirectory("Api_logs");
		response = Api_trigger.get_list_user_trigger(Data_repositery.headername(),
				Data_repositery.headervalue(), endpoint);
				Utility.evidenceFileCreator(log_dir, Utility.testLogName("get_list_user"), endpoint, null,
						response.getHeader("Date"), response.getBody().asPrettyString());
	}
	@When("trigger the api to hit the endpoint")
	public void trigger_the_api_to_hit_the_endpoint() {
		res_body = response.getBody().asPrettyString();
		statusCcode = response.getStatusCode();
		
		int idArr[] = new int[count];
		String emailArr[] = new String[count];
		String fnamesArr[] = new String[count];
		String lnamesArr[] = new String[count];

		for (int i = 0; i < count; i++) {
			{

				int res_id = res_jsn.getInt("data[" + i + "].id");
				idArr[i] = res_id;

				String res_email = res_jsn.get("data[" + i + "].email");
				emailArr[i] = res_email;

				String res_firstName = res_jsn.getString("data[" + i + "].first_name");
				fnamesArr[i] = res_firstName;

				String res_lastName = res_jsn.getString("data[" + i + "].last_name");
				lnamesArr[i] = res_lastName;
			}
			}

	}
	
	@Then("validate the status code")
	public void validate_the_status_code() {
		Assert.assertEquals(statusCcode, 200);
	}
	
	@Then("validate the all response body parameters")
	public void validate_the_all_response_body_parameters() {
		res_jsn = new JsonPath(res_body);
		count = res_jsn.getInt("data.size()");
		
		int idArr[] = new int[count];
		String emailArr[] = new String[count];
		String fnamesArr[] = new String[count];
		String lnamesArr[] = new String[count];

		for (int i = 0; i < count; i++) {
			{

				int res_id = res_jsn.getInt("data[" + i + "].id");
				idArr[i] = res_id;

				String res_email = res_jsn.get("data[" + i + "].email");
				emailArr[i] = res_email;

				String res_firstName = res_jsn.getString("data[" + i + "].first_name");
				fnamesArr[i] = res_firstName;

				String res_lastName = res_jsn.getString("data[" + i + "].last_name");
				lnamesArr[i] = res_lastName;
			}
		
		Assert.assertEquals(ids[i], idArr[i]);
		Assert.assertEquals(Emails[i], emailArr[i]);
		Assert.assertEquals(firstNames[i], fnamesArr[i]);
		Assert.assertEquals(lastNames[i], lnamesArr[i]);

	}

}}
	
