package common_trigger_methods;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListenerClass implements ITestListener{
	ExtentSparkReporter sparkreporter;
	ExtentReports extentreport;
	ExtentTest test;
	
	public void reportconfiguration() {
		sparkreporter=new ExtentSparkReporter(".\\extent-report\\report.html");
		extentreport = new ExtentReports();
		
		extentreport.attachReporter(sparkreporter);
		
//		adding sysytem information to report
		extentreport.setSystemInfo("OS", "Windows 10");
		extentreport.setSystemInfo("user", "Jayesh");
		
//		configuration for look of the report
		sparkreporter.config().setDocumentTitle("RestAssured_Extent_Listener_Report");
		sparkreporter.config().setReportName("Extent_Report");
		sparkreporter.config().setTheme(Theme.DARK);
	}

//	this method will get invoked before start of test case execution(same as @BeforeTest annotation)
	public void onStart(ITestContext result) {
		reportconfiguration();
		System.out.println("----on start method invoked----");
	}

	public void onFinish(ITestContext result) {
		System.out.println("----on finished method invoked----");
		extentreport.flush();
	}

//this method will get invoked whenever any of the test case fails
	public void onTestFailure(ITestResult result) {
		System.out.println("test method failed is:" + result.getName());
		test=extentreport.createTest(result.getName());
		test.log(Status.FAIL, MarkupHelper.createLabel("test method failed is:" + result.getName(), ExtentColor.RED));
	}

//this method will get invoked whenever any of the test case skip
	public void onTestSkipped(ITestResult result) {
		System.out.println("test method skipped is:" + result.getName());
		test= extentreport.createTest(result.getName());
		test.log(Status.SKIP, MarkupHelper.createLabel("test method skipped is:" + result.getName(), ExtentColor.YELLOW));
	}

//this method will get invoked on execution of each test case(same as @BeforeTest annotation)
	public void onTestStart(ITestResult result) {
		System.out.println("test method started is:" + result.getName());
	}

//this method will get invoked whenever any of the test case pass
	public void onTestSuccess(ITestResult result) {
		System.out.println("test method executed successfully is:" + result.getName());
		test =extentreport.createTest(result.getName());
		test.log(Status.PASS, MarkupHelper.createLabel("test method executed successfully is:" + result.getName(), ExtentColor.GREEN));
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
//	no implementation of this method as it is not being used in our project
	}



}
