endpoint is :
https://reqres.in/api/users

request body is:
{
    "name": "Swapneel",
    "job": "SrQA"
}

response header date is :
Sun, 17 Mar 2024 17:12:32 GMT

response body is :
{"name":"Swapneel","job":"SrQA","id":"164","createdAt":"2024-03-17T17:12:32.582Z"}

