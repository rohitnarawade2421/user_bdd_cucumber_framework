endpoint is :
https://reqres.in/api/users

request body is:
{
    "name": "morpheus",
    "job": "leader"
}

response header date is :
Sun, 17 Mar 2024 17:12:36 GMT

response body is :
{
    "name": "morpheus",
    "job": "leader",
    "id": "691",
    "createdAt": "2024-03-17T17:12:36.269Z"
}

